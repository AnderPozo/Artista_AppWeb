﻿using PracticaDataA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PracticaDataA.Controllers
{
    public class EventosController : Controller
    {
        private EventosDBContext db = new EventosDBContext();
        // GET: Eventos
        public ActionResult Index()
        {
            var eventos = from e in db.Eventosdb orderby e.Id select e;
            return View(eventos);
        }

        // GET: Eventos/Details/5
        public ActionResult Details(int id)
        {
            var eventos = db.Eventosdb.SingleOrDefault(e => e.Id == id);
            return View(eventos);
        }

        // GET: Eventos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Eventos/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                Eventos even = new Eventos();
                even.Nombre = collection["Nombre"];
                even.Categoria = collection["Categoria"];
                even.Informacion_pago = collection["Informacion_pago"];
                even.Lugar = collection["Lugar"];
                DateTime jdate;
                DateTime.TryParse(collection["Fecha"], out jdate);
                even.Fecha = jdate;

                db.Eventosdb.Add(even);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Eventos/Edit/5
        public ActionResult Edit(int id)
        {
            var eventos = db.Eventosdb.Single(m => m.Id == id);
            return View(eventos);
        }

        // POST: Eventos/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                var Eventos = db.Eventosdb.Single(m => m.Id == id);
                if (TryUpdateModel(Eventos))
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");

                }
                return View(Eventos);
            }
            catch
            {
                return View();
            }
        }

        // GET: Eventos/Delete/5
        public ActionResult Delete(int id)
        {
            var eventos = db.Eventosdb.Single(m => m.Id == id);
            return View(eventos);
        }

        // POST: Eventos/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Eventos even = db.Eventosdb.Find(id);
                db.Eventosdb.Remove(even);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
