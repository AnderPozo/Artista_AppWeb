﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PracticaDataA.Models
{
    public class Eventos
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Required]
        public string Categoria { get; set; }
        [Required]
        [Display(Name = "Gratuito o pagado")]
        public string Informacion_pago { get; set; }
        [Required]
        public string Lugar { get; set; }
        [Required]
        [Display(Name = "Fecha de creacion del evento")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }

    }

    public class EventosDBContext : DbContext
    {
        public EventosDBContext() { }
        public DbSet<Eventos> Eventosdb { get; set; }

    }
}